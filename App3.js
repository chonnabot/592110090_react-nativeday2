import React from 'react';
import { StyleSheet, Text, View, Modal, TouchableOpacity } from 'react-native';

class App3 extends React.Component {
    constructor(props) {
        super(props);
        this.state = { 
            isShow: false,
            message:" ",
        };
      }
      onShowModal = (message) =>{
          this.setState({ isShow: true, message})
      }
      onHideModal = () =>{
          this.setState({isShow: false,})
      }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <Text style={styles.headerText}>News</Text>
                </View>

                <View style={styles.content}>
                    <View style={styles.row}>
                        <View style={styles.box1}>
                            <TouchableOpacity>
                                <Text style={styles.headerText}
                                onPress={() => { this.onShowModal("Lorme1")}}
                                >Lorme1</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.box2}>
                            <TouchableOpacity>
                                <Text style={styles.headerText}
                                 onPress={() => { this.onShowModal("Lorme2")}}
                                 >Lorme2</Text>
                            </TouchableOpacity>
                        </View>
                    </View>


                    <View style={styles.row}>
                        <View style={styles.box1}>
                            <TouchableOpacity>
                                <Text style={styles.headerText}
                                 onPress={() => { this.onShowModal("Lorme3")}}
                                 >Lorme3</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.box2}>
                            <TouchableOpacity>
                                <Text style={styles.headerText}
                                 onPress={() => { this.onShowModal("Lorme4")}}
                                 >Lorme4</Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={styles.row}>
                        <View style={styles.box1}>
                            <TouchableOpacity>
                                <Text style={styles.headerText}
                                 onPress={() => { this.onShowModal("Lorme5")}}
                                 >Lorme5</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.box2}>
                            <TouchableOpacity>
                                <Text style={styles.headerText}
                                 onPress={() => { this.onShowModal("Lorme6")}}
                                 >Lorme6</Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                    <Modal
                        transparent={true}
                        visible={this.state.isShow}
                    >
                        <TouchableOpacity style={styles.modal}
                        onPress={() => { this.onHideModal()}}>
                            <Text style={styles.headerText}>{this.state.message}</Text>
                        </TouchableOpacity>  
                    </Modal>
                </View>

            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        backgroundColor: '#6633FF',
        flex: 1
    },
    header: {
        backgroundColor: '#6633FF',
        alignItems: 'center',
        flexDirection: 'column',
    },
    headerText: {
        color: 'white',
        fontSize: 20,
        fontWeight: 'bold',
        padding: 30,
    },
    content: {
        backgroundColor: '#6633FF',
        flex: 1,
        flexDirection: 'column',
    },
    box1: {
        backgroundColor: '#66FF00',
        flex: 1,
        margin: 14,
        flexDirection: 'column',
        alignItems: 'center',
    },
    box2: {
        backgroundColor: '#66FF00',
        flex: 1,
        margin: 14,
        flexDirection: 'column',
        alignItems: 'center',
    },
    row: {
        backgroundColor: '#6633FF',
        flex: 1,
        flexDirection: 'row',
    },
    modal: {
        backgroundColor: "rgba(192,192,192,0.6)",
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        margin: 30,
    },
});

export default App3



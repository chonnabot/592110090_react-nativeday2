import React from 'react';
import { StyleSheet, Text, View, Button, Alert, TouchableOpacity, TextInput, Image, ScrollView } from 'react-native';

class App2 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: 'Username',
            password: 'Password'
        };
    }
    onShowAlert = () =>{
        if(this.state.username === 'admin'){
            if(this.state.password === 'eiei'){
                return Alert.alert('Yahoo!!!')
            }
        }else   return Alert.alert('Username or Password wrong :( ')    
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <View style={styles.row}>
                        <View style={styles.column}>
                            <View style={styles.image}>
                                <Image
                                    style={styles.image}
                                    source={{ uri: 'https://scontent-kut2-2.xx.fbcdn.net/v/t1.0-9/50416171_2144023502324187_7446495404520112128_n.jpg?_nc_cat=111&_nc_ht=scontent-kut2-2.xx&oh=3c6f5d3ea9132af86cc030014e0f5681&oe=5CF6C7A7' }}
                                />

                            </View>
                        </View>
                    </View>
                </View>

                <View style={styles.body}>
                    <View style={styles.row}>
                        <View style={styles.column}>
                            <View style={styles.box1}>
                                <TextInput style={styles.boxText1}
                                    onChangeText={(value) => this.setState({ username: value })}
                                    placeholder='input username' />
                            </View>
                            <View style={styles.box1}>
                                <TextInput style={styles.boxText1}
                                    onChangeText={(password) => this.setState({ password })}
                                    placeholder='input password' />
                            </View>


                            <TouchableOpacity>
                                <Button title="Login" onPress={() => { this.onShowAlert()}} />
                            </TouchableOpacity>

                        </View>
                    </View>
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        backgroundColor: '#666',
        flex: 1
    },
    header: {
        backgroundColor: '#666',
        flex: 1
    },
    column: {                   //row
        backgroundColor: '#666',
        flexDirection: 'column',
        alignItems: 'center',
        flex: 1    //justifyContent

    },
    row: {                      //column
        backgroundColor: '#666',
        flexDirection: 'row',
        alignItems: 'center',
        flex: 1     //justifyContent

    },
    image: {
        backgroundColor: 'white',
        width: 200,
        height: 200,
        borderRadius: 100,
    },
    radiusText: {
        fontSize: 20,
        fontWeight: 'bold',
        padding: 70,
    },
    body: {
        backgroundColor: 'green',
        flex: 1
    },
    box1: {
        backgroundColor: 'white',
        flexDirection: 'column',
        alignItems: 'center',
        width: 300,
        height: 60,
        borderRadius: 10,
        margin: 14,
    },
    box2: {
        backgroundColor: '#330000',
        flexDirection: 'column',
        alignItems: 'center',
        width: 300,
        height: 60,
        borderRadius: 10,
        margin: 14,
    },
    boxText1: {
        fontSize: 20,
        fontWeight: 'bold',
        padding: 10,
    },
    boxText2: {
        color: 'white',
        fontSize: 20,
        fontWeight: 'bold',
        padding: 10,
    },
});

export default App2



